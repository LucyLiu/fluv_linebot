//程式碼開始
var　CHANNEL_ACCESS_TOKEN　=　"";
var　spreadSheetId　=　"";　　　　//試算表　ID
var　sheetName　=　"";　　　　//工作表名稱
var　searchColumn　=　2;　　　　//搜尋第幾欄的資料
var　allowUserId　=　[""];　　//允許取得資料的使用者ID
var　spreadSheet　=　SpreadsheetApp.openById(spreadSheetId);
var　sheet　=　spreadSheet.getSheetByName(sheetName);
var　lastRow　=　sheet.getLastRow();
var　lastColumn　=　sheet.getLastColumn();
var　sheetData　=　sheet.getSheetValues(1,　1,　lastRow,　lastColumn);
function　doPost(e)　{
　　var　userData　=　JSON.parse(e.postData.contents);
　　var　allowed　=　false;
　　var　clientID　=　userData.events[0].source.userId;
　　//　檢查是否是允許的用者提出搜尋需求
　　for　(var　i　=　0;　i　<　allowUserId.length;　i++)　{
　　　　if　(allowUserId[i]　==　clientID)　{
　　　　　　allowed　=　true;
　　　　　　break;
　　　　}
　　}
　　if　(!allowed)　{return;}
　　var　searchResult　=　[];
　　var　replyMessage　=　[];
　　var　replyContent;
　　var　replyToken　=　userData.events[0].replyToken;
　　var　searchContent　=　userData.events[0].message.text;
　　if　(userData.events[0].type　!=　"message")　{return;}
　　if　(userData.events[0].message.type　!=　"text")　{return;}
　　searchResult　=　sheetData.filter(function(item,　index,　array){
　　　　return　item[searchColumn　-　1].toString()　===　searchContent;
　　});
　　for　(var　i　=　0;　i　<　searchResult.length;　i++)　{
　　　　replyContent　=　sheetData[0][0]　+　"："　+　searchResult[i][0];
　　　　for　(var　j　=　1;　j　<　lastColumn;　j++)　{
　　　　　　replyContent　+=　"\n\n"　+　sheetData[0][j]　+　"："　+　searchResult[i][j];
　　　　}
　　　　replyMessage.push({type:"text",　text:replyContent});
　　　　if　(replyMessage.length　==　5)　{break;}
　　}
　　if　(replyMessage.length　==　0)　{replyMessage.push({type:"text",　text:"查詢不到「"　+　searchContent　+　"」的資料"});}
　　sendReplyMessage(CHANNEL_ACCESS_TOKEN,　replyToken,　replyMessage);
}
//回送　Line　Bot　訊息給使用者
function　sendReplyMessage(CHANNEL_ACCESS_TOKEN,　replyToken,　replyMessage)　{
　　var　url　=　"https://api.line.me/v2/bot/message/reply";
　　UrlFetchApp.fetch(url,　{
　　　　"headers":　{
　　　　　　"Content-Type":　"application/json;　charset=UTF-8",
　　　　　　"Authorization":　"Bearer　"　+　CHANNEL_ACCESS_TOKEN,
　　　　},
　　　　"method":　"post",
　　　　"payload":　JSON.stringify({
　　　　　　"replyToken":　replyToken,
　　　　　　"messages":　replyMessage,
　　　　}),
　　});
}
//程式碼結束