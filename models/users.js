import {doc, creds} from '../config'
async function getOnlineAdoption() {
  await doc.useServiceAccountAuth({
    client_email: creds.client_email,
    private_key: creds.private_key,
  });

  await doc.loadInfo(); // loads document properties and worksheets  
  const onlineAdoption = doc.sheetsById['901501433'];
  return onlineAdoption
}

export async function getUser() {
  return getOnlineAdoption.title
}

export async function setUser() {

}

export async function updateUser() {

}