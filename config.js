const { GoogleSpreadsheet } = require('google-spreadsheet');

// Initialize the sheet - doc ID is the long id in the sheets URL
export const doc = new GoogleSpreadsheet('1CU1tw7_w5Ouf3BP4nL4jlMps368r7FX2GXk2uINDnoA');

// Initialize Auth - see more available options at https://theoephraim.github.io/node-google-spreadsheet/#/getting-started/authentication

export const creds = {
  "type": "service_account",
  "project_id": "fluv-dev",
  "private_key_id": "8692379889ec6965cd938f51e21beac5a41a2c9f",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCRaAqp2F+AdUOb\nff8ZaShVsRtk0W6wFHK5fTTN4MVIO2vYyq3ZpkAMFiuvk4hIQXLy6E8S0kIrIWqr\nuL4PrpKVtoax7tkQ53PWHHPXtwoCR+TvbMOK3B7PhsZcRwR5Tc9xLEDHZJQATZkx\nrStMAhDz6otd26mpswkSUlYJa2NEHA+lUaF8lsks+686In/9cr/5vaqUiUOvQwXM\nKy0qRcox1G1K6s5KliBMhLfLVdjgT748lzi5CzNnKlJ8yE7s1ytQ7aFYewP9QU+P\nFco1ta7ci/9BU5/SrfMQxFjyFOoQynGv+kOGVcGYP2yqRcjFL4N2rDBeY+/VK+yH\n8IfS9BtZAgMBAAECggEACYswI/y5K/fGY7bN3w+ZX6J7v07FupXk/8q5hAfMOxT3\nW6fV6FKyonh0Dpaa28876biYtA4L3MPZOgMqqmHVajEsPxKDucnPPw7oBZEUfZjV\nbGeRoBQZi5p3e4gHwaR7SJzUdaHi2vA87VdF37vE/BV5Z0QrKyG+py6zSfP79HTf\nrGsla66ad74lkYlrp6Q+QCztvNheCjO7S7lBcHFAIZMRHP/znJpiMHK3kMEkB/2z\nxHbtsqyEjTuG5n1+t6TfDRoUuXdP9LSG5HDB32YodaM0f7ayYqzKc43GGGzq8oN1\nWReTpfbEYkvX/P108YXd2YC0lSNe/eBLEf2aXwpTwQKBgQDM/kC45GQfZ627MHVm\nszUTqkJuig3SMKZnnuncxizzXRtBO9NwxlS2jnJD4CgXlW5I6me7j1hEaFjuKP/N\n3vD/MNCxW20VtOXmAaVd3uIHcZXc4nKxz4LbsH7iDVsheFbAyV2+kXC/q5NQsJn5\nCHg0tmvsfqFSgbjzpboMQc4hCQKBgQC1ljRb2pWgUS+F5V+KXPbT86PyhlMovAOj\nD0HHy1e9wZPns/imPVg/w+j4fPJA8USuxaGY+sn7XAMq5ScW5w+VI8PqJz9r4ttV\nBaYWGAmcqG4EcJopWwfKuypvpo/7g5FtM1RfumWvpHfWxS7V9SWQq6+FjqUtVd9K\nhSb1LPHL0QKBgQCWYUp/mGRuuXM0aBcn8P1V4Mf9KV98O15Hk8eBwwvEuB+/WjEB\ngS0yAwuGCZG/w0p1SSgx6rVY8aeThxYpI4xt2JSwFx7OFre0ZK64T0JHLac/tAtp\nr44StcZO9HdO9WXG5bHUlAM/SJG4VuWNUA5GtTA0/KxBx5IzgvcZ+MhLaQKBgQCZ\naH9thnpYSZ5TiVew5h8lKzW7492HkWelL3OMXm8PAeZ5nqMXlsXigRlmLPDhCrPS\nz2ZZfq57a93FNHlUsPqKLgAK5wN+mtZfuvj4u3ALaIQhvhuPirdHf943G0mS3DFV\nR0Ml6KyVueYAjO+nlIca9ODBfaRwa5bMNzeVdFYGsQKBgBRWz6iq1jfr3w84EKXv\nmTObUbzAPqbBMKnIYK/+XSjagsB/fZvkdUcEAiVa2RSO8+934fprPxYNAgU7VbN0\nP7vGNxroeWfJ5gYiYLgYGK6JXSU1uMEGQgzMcpJrKNXxOkHFPcnYyg3P47eHpwEA\nQAbyZ/L/t60+R5mTW9d3xxxH\n-----END PRIVATE KEY-----\n",
  "client_email": "google-drive-api@fluv-dev.iam.gserviceaccount.com",
  "client_id": "108827325154729585747",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/google-drive-api%40fluv-dev.iam.gserviceaccount.com"
};
